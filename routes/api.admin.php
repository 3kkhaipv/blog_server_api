<?php
use Illuminate\Support\Facades\Route;

Route::group(['as' => 'admin.'], function () {
    Route::post('login', 'AuthenticateController@login')->name('login');

    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::post('logout', 'AuthenticateController@logout')->name('logout');

        Route::apiResource('users', 'UserController');

        Route::apiResource('categories', 'CategoryController');

        Route::apiResource('posts', 'PostController');
    });
});
