<?php

namespace Si\L6Core;

use Illuminate\Support\ServiceProvider;
use Si\L6Core\Commands\MakeRepository;
use Si\L6Core\Commands\MakeService;
use Si\L6Core\Commands\MakeFilter;
use Si\L6Core\Commands\MakeCriteria;

class L6CoreServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                MakeRepository::class,
                MakeService::class,
                MakeFilter::class,
                MakeCriteria::class
            ]);
        }
    }
}
