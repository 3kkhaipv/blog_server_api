<?php

namespace App\Repositories;

use App\Models\Category;
use Si\L6Core\Repositories\BaseRepository;

class CategoryRepository extends BaseRepository
{
    /**
     * Get the model of repository
     *
     * @return string
     */
    public function getModel()
    {
        return Category::class;
    }

    public function getOrderableFields()
    {
        return [
            'id',
            'name',
            'created_at',
            'updated_at'
        ];
    }
}
