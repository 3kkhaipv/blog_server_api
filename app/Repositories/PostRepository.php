<?php

namespace App\Repositories;

use App\Models\Post;
use Si\L6Core\Repositories\BaseRepository;

class PostRepository extends BaseRepository
{
    /**
     * Get the model of repository
     *
     * @return string
     */
    public function getModel()
    {
        return Post::class;
    }

    public function getOrderableFields()
    {
        return [
            'id',
            'title',
            'detail',
            'created_at',
            'updated_at'
        ];
    }
}
