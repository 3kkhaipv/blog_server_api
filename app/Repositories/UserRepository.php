<?php

namespace App\Repositories;

use App\Models\User;
use Si\L6Core\Repositories\BaseRepository;

class UserRepository extends BaseRepository
{
    /**
     * Get the model of repository
     *
     * @return string
     */
    public function getModel()
    {
        return User::class;
    }

    public function getOrderableFields()
    {
        return [
            'id',
            'name',
            'email',
            'created_at',
            'updated_at'
        ];
    }
}
