<?php

namespace App\Helpers;

class UploadHelper
{
    public function uploadImage($file)
    {
        try {
            $imageName = '';
            if (!is_null($file)) {
                $imageName = 'BLOG-' . time() . rand() . '.' . $file->getClientOriginalExtension();
                $file->move('uploads', $imageName);
            }

            return url('uploads/' . $imageName);
        } catch (\Exception $exception) {
            throw $exception;
        }

    }

    public function removeImage($path)
    {
        $name  = explode('/', $path);
        $oldImage = public_path('uploads/' . $name[4]);
        if (file_exists($oldImage)) {
            unlink($oldImage);
        }
    }
}
