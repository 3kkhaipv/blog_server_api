<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\CreateUserRequest;
use App\Http\Requests\Admin\User\ListUserRequest;
use App\Http\Requests\Admin\User\UpdateUserRequest;
use App\Http\Resources\Admin\User\UserCollection;
use App\Http\Resources\Admin\User\UserResource;
use App\Services\Admin\User\CreateUserService;
use App\Services\Admin\User\DeleteUserService;
use App\Services\Admin\User\ListUserService;
use App\Services\Admin\User\UpdateUserService;

class UserController extends Controller
{
    public function index(ListUserRequest $request, ListUserService $service)
    {
        $result = $service->setRequest($request)->handle();

        return response()->success(new UserCollection($result));
    }

    public function store(CreateUserRequest $request, CreateUserService $service)
    {
        $result = $service->setRequest($request)->handle();

        return response()->created(new UserResource($result));
    }

    public function update(UpdateUserRequest $request, UpdateUserService $service, $id)
    {
        $result = $service->setRequest($request)->setModel($id)->handle();

        return response()->success(new UserResource($result));
    }

    public function destroy($id, DeleteUserService $service)
    {
        $service->setModel($id)->handle();

        return response()->successWithoutData();
    }
}
