<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Category\CreateCategoryRequest;
use App\Http\Requests\Admin\Category\ListCategoryRequest;
use App\Http\Requests\Admin\Category\UpdateCategoryRequest;
use App\Http\Resources\Admin\Category\CategoryCollection;
use App\Http\Resources\Admin\Category\CategoryResource;
use App\Services\Admin\Category\CreateCategoryService;
use App\Services\Admin\Category\DeleteCategoryService;
use App\Services\Admin\Category\FindCategoryService;
use App\Services\Admin\Category\ListCategoryService;
use App\Services\Admin\Category\UpdateCategoryService;

class CategoryController extends Controller
{
    public function index(ListCategoryRequest $request, ListCategoryService $service)
    {
        $result = $service->setRequest($request)->handle();

        return response()->success(new CategoryCollection($result));
    }

    public function show($id, FindCategoryService $service)
    {
        $result = $service->setModel($id)->handle();

        return response()->created(new CategoryResource($result));
    }

    public function store(CreateCategoryRequest $request, CreateCategoryService $service)
    {
        $result = $service->setRequest($request)->handle();

        return response()->created(new CategoryResource($result));
    }

    public function update(UpdateCategoryRequest $request, UpdateCategoryService $service, $id)
    {
        $result = $service->setRequest($request)->setModel($id)->handle();

        return response()->success(new CategoryResource($result));
    }

    public function destroy($id, DeleteCategoryService $service)
    {
        $service->setModel($id)->handle();

        return response()->successWithoutData();
    }
}
