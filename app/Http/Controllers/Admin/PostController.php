<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Post\CreatePostRequest;
use App\Http\Requests\Admin\Post\ListPostRequest;
use App\Http\Requests\Admin\Post\UpdatePostRequest;
use App\Http\Resources\Admin\Post\PostCollection;
use App\Http\Resources\Admin\Post\PostResource;
use App\Services\Admin\Post\CreatePostService;
use App\Services\Admin\Post\DeletePostService;
use App\Services\Admin\Post\FindPostService;
use App\Services\Admin\Post\ListPostService;
use App\Services\Admin\Post\UpdatePostService;

class PostController extends Controller
{
    public function index(ListPostRequest $request, ListPostService $service)
    {
        $result = $service->setRequest($request)->handle();

        return response()->success(new PostCollection($result));
    }

    public function show($id, FindPostService $service)
    {
        $result = $service->setModel($id)->handle();

        return response()->created(new PostResource($result));
    }

    public function store(CreatePostRequest $request, CreatePostService $service)
    {
        $result = $service->setRequest($request)->handle();

        return response()->created(new PostResource($result));
    }

    public function update(UpdatePostRequest $request, UpdatePostService $service, $id)
    {
        $result = $service->setRequest($request)->setModel($id)->handle();

        return response()->success(new PostResource($result));
    }

    public function destroy($id, DeletePostService $service)
    {
        $service->setModel($id)->handle();

        return response()->successWithoutData();
    }
}
