<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\LoginRequest;
use App\Http\Requests\Admin\LogoutRequest;
use App\Http\Resources\Admin\LoginResource;
use App\Services\Admin\LoginService;
use App\Services\Admin\LogoutService;
use Illuminate\Http\Request;

class AuthenticateController extends Controller
{
    public function login(LoginRequest $request, LoginService $loginService)
    {
        $result = $loginService->setRequest($request)->handle();

        return response()->success(new LoginResource($result));
    }

    public function logout(LogoutRequest $request, LogoutService $logoutService)
    {
        $logoutService->setRequest($request)->handle();

        return response()->successWithoutData();
    }
}
