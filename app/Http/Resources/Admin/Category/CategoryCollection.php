<?php

namespace App\Http\Resources\Admin\Category;

use App\Http\Resources\PaginatedCollection;

class CategoryCollection extends PaginatedCollection
{
}
