<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/** @SuppressWarnings(PHPMD.NumberOfChildren) */
class BaseRequest extends FormRequest
{
    const MAX_LENGTH_EMAIL = 150;

    const MIN_LENGTH_PASSWORD = 6;
    const MAX_LENGTH_PASSWORD = 50;
    const PASSWORD_REGEX = "/^(?=.*[0-9])(?=.*[a-zA-Z])(.+){6,50}$/";

    const LIST_ORDER = 'order';
    const LIST_SEARCH = 'search';
    const LIST_PER_PAGE = 'per_page';
    const LIST_PAGE = 'page';

    const SEARCH_DEFAULT_LENGTH = 100;
    const INT_32_MIN = 0;
    const LIMIT_DEFAULT_MAX = 500;
    const ORDER_DEFAULT_LENGTH = 50;
    const WITH_DEFAULT_LENGTH = 200;


    public function commonListRules()
    {
        return [
            self::LIST_SEARCH => [
                'bail',
                'sometimes',
                'nullable',
                'string',
                'max:' . self::SEARCH_DEFAULT_LENGTH
            ],
            self::LIST_PAGE => [
                'bail',
                'sometimes',
                'integer',
            ],
            self::LIST_PER_PAGE => [
                'bail',
                'sometimes',
                'integer',
                'min:' . self::INT_32_MIN,
                'max:' . self::LIMIT_DEFAULT_MAX
            ],
            self::LIST_ORDER => [
                'bail',
                'sometimes',
                'string',
                'max:' . self::ORDER_DEFAULT_LENGTH
            ],
            'with' => [
                'bail',
                'sometimes',
                'string',
                'max:' . self::WITH_DEFAULT_LENGTH
            ]
        ];
    }
}
