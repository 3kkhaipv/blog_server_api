<?php

namespace App\Filters;

use Si\L6Core\Filters\BaseFilter;

class CreatedAtLte extends BaseFilter
{
    /**
     * Apply the filter
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param mixed $input
     * @return mixed
     */
    public static function apply($model, $input)
    {
        return $model->whereDate('created_at', '<=', $input);
    }
}
