<?php

namespace App\Filters;

use Si\L6Core\Filters\BaseFilter;

class SearchUser extends BaseFilter
{
    /**
     * Apply the filter
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param mixed $input
     * @return mixed
     */
    public static function apply($model, $input)
    {
        return $model->where(function ($query) use ($input) {
            return $query->where('email', 'like', "%$input%")
                ->orWhere('name', 'like', "%$input%");
        });
    }
}
