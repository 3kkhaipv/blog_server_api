<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Si\L6Core\Exceptions\BaseException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param Exception $exception
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param Exception $e
     * @return Response
     * @throws Exception
     */
    public function render($request, Exception $e)
    {
        $statusCode  = 400;
        $errors      = [];
        $message     = __('messages.errors.unexpected');
        $messageCode = '';

        switch (true) {
            case $e instanceof ValidationException:
                $errorMsg = collect($e->errors())->first();
                $message    = !empty($errorMsg) ? reset($errorMsg) : __('messages.errors.input');
                $errors     = $e->errors();
                $statusCode = 422;
                $messageCode = 'errors.input';
                break;

            case $e instanceof NotFoundHttpException:
            case $e instanceof MethodNotAllowedHttpException:
            case $e instanceof AccessDeniedHttpException:
            case $e instanceof AuthorizationException:
                $message    = __('messages.errors.route');
                $statusCode = 404;
                $messageCode = 'route.not_found';
                break;

            case $e instanceof ModelNotFoundException:
                $message = __('messages.errors.data');
                $statusCode = 404;
                $messageCode = 'record.not_found';
                break;

            // case $e instanceof JWTException:
            // case $e instanceof TokenInvalidException:
            // case $e instanceof TokenBlacklistedException:
            case $e instanceof AuthenticationException:
                $message = __('messages.errors.session');
                $statusCode = 401;
                $messageCode = 'session.not_found';
                break;

            case $e instanceof ThrottleRequestsException:
                $message = __('messages.errors.many_attempts');
                $statusCode = 429;
                $messageCode = 'request.max_attemps';
                break;

            case $e instanceof BaseException:
                // case $e instanceof VoipException:
                // case $e instanceof PaymentException:
                // case $e instanceof CallingTalkException:
                $message     = $e->getMessage();
                $messageCode = method_exists($e, 'getMessageCode') ? $e->getMessageCode() : null;
                $statusCode  = $e->getCode();
                break;

            default:
                break;
        }

        $data = [
            'data'   => $errors,
            'code'   => $messageCode,
            'message'=> $message
        ];

        return $request->is('api/*') ? response()->json($data, $statusCode) : parent::render($request, $e);
    }
}
