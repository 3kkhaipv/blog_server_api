<?php

namespace App\Exceptions;

use Si\L6Core\Exceptions\BaseException;

class LoginException extends BaseException
{
    public static function invalidCredentialsAdmin()
    {
        return self::code('errors.admin_login_info');
    }
}
