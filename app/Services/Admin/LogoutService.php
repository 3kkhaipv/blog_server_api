<?php

namespace App\Services\Admin;

use Si\L6Core\Services\BaseService;
use Tymon\JWTAuth\Facades\JWTAuth;

class LogoutService extends BaseService
{
    protected $collectsData = true;
    /**
     * Logic to handle the data
     */
    public function handle()
    {
        JWTAuth::invalidate($this->data->get('token'));

        return;
    }
}
