<?php

namespace App\Services\Admin\User;

use App\Repositories\UserRepository;
use Si\L6Core\Services\BaseService;

class UpdateUserService extends BaseService
{
    protected $collectsData = true;

    protected $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        $password = bcrypt($this->data->get('password'));
        $this->data->put('password', $password);

        return $this->repository->update($this->model, $this->data->toArray());
    }
}
