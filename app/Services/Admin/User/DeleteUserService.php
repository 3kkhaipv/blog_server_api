<?php

namespace App\Services\Admin\User;

use App\Repositories\UserRepository;
use Si\L6Core\Services\BaseService;

class DeleteUserService extends BaseService
{
    protected $collectsData = true;

    protected $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        return $this->repository->delete($this->model);
    }
}
