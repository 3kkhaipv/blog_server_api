<?php

namespace App\Services\Admin\Post;

use App\Helpers\UploadHelper;
use App\Repositories\PostRepository;
use Si\L6Core\Services\BaseService;

class UpdatePostService extends BaseService
{
    protected $collectsData = true;

    protected $repository;

    protected $uploadHelper;

    public function __construct(PostRepository $repository, UploadHelper $uploadHelper)
    {
        $this->repository = $repository;
        $this->uploadHelper = $uploadHelper;
    }

        /**
     * Logic to handle the data
     */
    public function handle()
    {
        if (!empty($this->data->get('image'))) {
            $post = $this->repository->find($this->model);
            $this->uploadHelper->removeImage($post->image);
            $params = [
                'title' => $this->data->get('title'),
                'image' => $this->uploadHelper->uploadImage($this->data->get('image')),
                'detail' => $this->data->get('detail'),
                'category_id' => $this->data->get('category_id'),
            ];

            return $this->repository->update($this->model, $params);
        }

        return $this->repository->update($this->model, $this->data->toArray());
    }
}
