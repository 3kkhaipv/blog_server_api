<?php

namespace App\Services\Admin\Post;

use App\Helpers\UploadHelper;
use App\Repositories\PostRepository;
use Si\L6Core\Services\BaseService;

class CreatePostService extends BaseService
{
    protected $collectsData = true;

    protected $repository;

    protected $uploadHelper;

    public function __construct(PostRepository $repository, UploadHelper $uploadHelper)
    {
        $this->repository = $repository;
        $this->uploadHelper = $uploadHelper;
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        $params = [
          'title' => $this->data->get('title'),
          'image' => $this->uploadHelper->uploadImage($this->data->get('image')),
          'detail' => $this->data->get('detail'),
          'category_id' => $this->data->get('category_id'),
        ];

        return $this->repository->create($params);
    }
}
