<?php

namespace App\Services\Admin\Post;

use App\Helpers\UploadHelper;
use App\Repositories\PostRepository;
use Si\L6Core\Services\BaseService;

class DeletePostService extends BaseService
{
    protected $collectsData = true;

    protected $repository;

    protected $uploadHelper;

    public function __construct(PostRepository $repository, UploadHelper $uploadHelper)
    {
        $this->repository = $repository;
        $this->uploadHelper = $uploadHelper;
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        $post = $this->repository->find($this->model);
        $this->uploadHelper->removeImage($post->image);

        return $this->repository->delete($this->model);
    }
}
