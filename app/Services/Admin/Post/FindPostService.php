<?php

namespace App\Services\Admin\Post;

use App\Repositories\PostRepository;
use Si\L6Core\Services\BaseService;

class FindPostService extends BaseService
{
    protected $collectsData = true;

    protected $repository;

    public function __construct(PostRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        return $this->repository->find($this->model);
    }
}
