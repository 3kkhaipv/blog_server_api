<?php

namespace App\Services\Admin\Post;

use App\Filters\CreatedAtGte;
use App\Filters\CreatedAtLte;
use App\Filters\SearchPost;
use App\Repositories\PostRepository;
use Si\L6Core\Criteria\FilterCriteria;
use Si\L6Core\Criteria\OrderCriteria;
use Si\L6Core\Services\BaseService;

class ListPostService extends BaseService
{
    protected $collectsData = true;

    protected $repository;

    public function __construct(PostRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        $this->repository
            ->pushCriteria(new FilterCriteria($this->data->toArray(), $this->allowFilters()))
            ->pushCriteria(new OrderCriteria($this->data->get('order')));

        return $this->data->has('per_page')
            ? $this->repository->paginate((int)$this->getPerPage())
            : $this->repository->all();
    }

    private function allowFilters()
    {
        return [
            'search' => SearchPost::class,
            'created_at_gte' => CreatedAtGte::class,
            'created_at_lte' => CreatedAtLte::class,
        ];
    }
}
