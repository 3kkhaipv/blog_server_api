<?php

namespace App\Services\Admin\Category;

use App\Filters\CreatedAtGte;
use App\Filters\CreatedAtLte;
use App\Filters\SearchCategory;
use App\Repositories\CategoryRepository;
use Si\L6Core\Criteria\FilterCriteria;
use Si\L6Core\Criteria\OrderCriteria;
use Si\L6Core\Services\BaseService;

class ListCategoryService extends BaseService
{
    protected $collectsData = true;

    protected $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        $this->repository
            ->pushCriteria(new FilterCriteria($this->data->toArray(), $this->allowFilters()))
            ->pushCriteria(new OrderCriteria($this->data->get('order')));

        return $this->data->has('per_page')
            ? $this->repository->paginate((int)$this->getPerPage())
            : $this->repository->all();
    }

    private function allowFilters()
    {
        return [
            'search' => SearchCategory::class,
            'created_at_gte' => CreatedAtGte::class,
            'created_at_lte' => CreatedAtLte::class,
        ];
    }
}
