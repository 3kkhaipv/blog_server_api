<?php

namespace App\Services\Admin\Category;

use App\Repositories\CategoryRepository;
use Si\L6Core\Services\BaseService;

class DeleteCategoryService extends BaseService
{
    protected $collectsData = true;

    protected $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        return $this->repository->delete($this->model);
    }
}
