<?php

namespace App\Services\Admin;

use App\Exceptions\LoginException;
use Si\L6Core\Services\BaseService;
use Tymon\JWTAuth\Facades\JWTAuth;

class LoginService extends BaseService
{
    protected $collectsData = true;

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        $credentials = [
            'email' => $this->data->get('email'),
            'password' => $this->data->get('password')
        ];

        $token = JWTAuth::attempt($credentials);
        if (empty($token)) {
            throw LoginException::invalidCredentialsAdmin();
        }
        return [
            'access_token' =>  $token
        ];
    }
}
